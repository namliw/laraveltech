<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quantity')->unsigned();#qty can't be negative
            $table->integer('product_id')->unsigned()->index();
            $table->integer('order_id')->unsigned()->index();
            $table->string('product_sku')->index();
            $table->enum('status', ['Available', 'Assigned']);
            $table->enum('physical_status', ['To order','In warehouse', 'Delivered']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
