# Origin aLaravel Quickstart - Basic Instructions

## Quck Installation

    git clone https://github.com/laravel/quickstart-basic quickstart

    cd quickstart

    composer install

    php artisan migrate

    php artisan serve

[Complete Tutorial](https://laravel.com/docs/5.2/quickstart)

# Provided Instructions

- Add .env file and your mysql connection details

        APP_ENV=local
        APP_DEBUG=true
        APP_KEY=b809vCwvtawRbsG0BmP1tWgnlXQypSKf
        APP_URL=http://localhost
        
        DB_HOST=127.0.0.1
        DB_DATABASE=homestead
        DB_USERNAME=[user]
        DB_PASSWORD=[password]
        
        CACHE_DRIVER=file
        SESSION_DRIVER=file
        QUEUE_DRIVER=sync
        
        REDIS_HOST=127.0.0.1
        REDIS_PASSWORD=null
        REDIS_PORT=6379
        
        MAIL_DRIVER=smtp
        MAIL_HOST=mailtrap.io
        MAIL_PORT=2525
        MAIL_USERNAME=null
        MAIL_PASSWORD=null
        MAIL_ENCRYPTION=null

- run a composer install
- Configure your host like any laravel project (Document root to public folder in apache conf).
- create the database to use
- run migrations

##crontasks

Jobs where created as artisan commands.

- run

        sudo -u www-data crontab -e

- then add

        * * * * * cd /var/www/projects/www.brossa.local && php artisan order:cancel > /tmp/cancelOrderLog.log
        * * * * * cd /var/www/projects/www.brossa.local && php artisan order:complete > /tmp/completeOrderLog.log
        
##api calls

The follwing are sample calls to invoke the api calls created.

- Create new order

        curl -H "Content-Type: application/json" -X POST -d '{"order":{"customer":"dude 1","address":"somewhere i belong","total":100,"items":[{"sku":"asdasdas","quantity":2},{"sku":"test","quantity":2}]}}' http://brossa.solocode.it/api/orders/create

- Assign items to order (change {id} in url)

        curl -H "Content-Type: application/json" -X POST -d '{"items":[{"sku":"asdasdas","quantity":2},{"sku":"test","quantity":2},{"sku":"proper","quantity":2}]}' http://brossa.solocode.it/api/orders/{id}/assign_items

- Remove items from order (change {id} in url)

        curl -H "Content-Type: application/json" -X POST -d '{"items":["asdasdas","test"]}' http://brossa.solocode.it/api/orders/{id}/remove_items