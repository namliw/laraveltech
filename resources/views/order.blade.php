@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">

            <div class="panel panel-default">
                <div class="panel-heading">
                    Order {{$order->id}}
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')
                    Customer name: {{$order->customer}} <br />
                    Address: {{$order->address}} <br />
                    Status: {{$order->status}} <br />
                    <br />
                    @if($order->isCancelled())
                        <form action="{{ url('order/'.$order->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button type="submit" class="btn btn-danger">
                                <i class="fa fa-btn fa-trash"></i>Delete
                            </button>
                        </form>
                    @endif
                </div>
            </div>

            <!-- Current orders -->
            @if (count($order->items) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Orders
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped order-table">
                            <thead>
                                <th>Product name</th>
                                <th>&nbsp;SKU </th>
                                <th>&nbsp;Quantity </th>
                                <th>&nbsp;Status</th>
                                <th> Physical status</th>
                                <th> </th>
                                <th> </th>
                            </thead>
                            <tbody>
                                @foreach ($order->items as $item)
                                    <tr>
                                        <td class="table-text"><div><A href="{{url('/products/'.$item->product->id)}}">{{ $item->product->name }}</A></div></td>
                                        <td class="table-text"><div>{{ $item->product_sku }}</div></td>
                                        <td class="table-text"><div>{{ $item->quantity}}</div></td>
                                        <td class="table-text"><div>{{$item->status}}</div></td>
                                        <td class="table-text"><div>{{$item->physical_status}}</div></td>

                                        <!-- order Delete Button -->
                                        <td>
                                            @unless($item->isDelivered())
                                                <form action="{{ url('items/detach_item/'.$item->id) }}" method="POST">
                                                    {{ csrf_field() }}

                                                    <button type="submit" class="btn btn-danger">
                                                        <i class="fa fa-btn fa-trash"></i>Detach
                                                    </button>
                                                </form>
                                            @endunless
                                        </td>
                                        <td>
                                            @if($item->isPendingDelivery())
                                                <form action="{{ url('items/deliver_item/'.$item->id) }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="fa fa-btn"></i>Mark as delivered
                                                    </button>
                                                </form>
                                            @elseif($item->isPendingItemOrder())
                                                <form action="{{ url('items/ordered_item/'.$item->id) }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="fa fa-btn"></i>Mark as ordered
                                                    </button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
