@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <!-- Display Validation Errors -->
        @include('common.errors')
        <!-- Current orders -->
            @if (count($orders) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Orders
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped order-table">
                            <thead>
                            <th>Order</th>
                            <th>Customer name</th>
                            <th>Address&nbsp;</th>
                            <th>Status</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            </thead>
                            <tbody>
                            @foreach ($orders as $order)
                                <tr>
                                    <td class="table-text">
                                        <div><A href="{{url('/orders/'.$order->id)}}">{{ $order->id }}</A></div>
                                    </td>
                                    <td class="table-text">
                                        <div>{{ $order->customer_name }}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>{{ $order->address}}</div>
                                    </td>
                                    <td class="table-text">
                                        <div>{{ $order->status}}</div>
                                    </td>

                                    <!-- order Delete Button -->
                                    <td>
                                        @if($order->isCancelled())
                                            <form action="{{ url('order/'.$order->id) }}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" class="btn btn-danger">
                                                    <i class="fa fa-btn fa-trash"></i>Delete
                                                </button>
                                            </form>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ url('/orders/'.$order->id) }}" class="btn btn-primary">
                                            <i class="fa fa-btn"></i>View details
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
