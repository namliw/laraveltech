@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Product {{$product->name}}
                </div>

                <div class="panel-body">
                    <!-- Display Validation Errors -->
                    @include('common.errors')

                    <form class="col-sm-12" action="{{ url('products/update/'.$product->id) }}" method="POST" autocomplete="off">
                        {{ csrf_field() }}
                        <div clas="col-sm-12">
                            <h4>Edit product information</h4>
                        </div>
                        <label for="product-name" class="col-sm-3 control-label">
                            name
                            <input type="text" name="name" id="product-name" value="{{ old('name',  isset($product->name) ? $product->name : null) }}" class="form-control" />
                        </label>

                        <label for="product-sku" class="col-sm-3 control-label">
                            sku
                            <input type="text" name="sku" id="product-sku" value="{{ old('sku',  isset($product->sku) ? $product->sku : null) }}" class="form-control" />
                        </label>

                        <label for="product-colour" class="col-sm-3 control-label">
                            colour
                            <input type="text" name="colour" id="product-colour" value="{{ old('colour',  isset($product->colour) ? $product->sku : null) }}" class="form-control" />
                        </label>
                        <div class="col-sm-6">
                            <button type="submit" class="btn">
                                <i class="fa fa-btn fa-plus"></i>Update product
                            </button>
                        </div>
                    </form>

                    <form class="col-sm-12" action="{{ url('products/addItems/'.$product->id) }}" method="POST">
                        {{ csrf_field() }}

                        <div clas="col-sm-12">
                            <br />
                            <h4>Add items to product</h4>
                        </div>

                        <label for="product-items" class="col-sm-3 control-label">
                            Quantity
                            <input type="text" name="items" id="product-items" class="form-control" />
                        </label>

                        <label for="product-status" class="col-sm-3 control-label">
                            Physical status
                            <select name="physical_status" class="dropdown" id="product-status">
                                <option value="To order">To order</option>
                                <option value="In warehouse">In warehouse</option>
                            </select>
                        </label>

                        <div class="col-sm-6">
                            <button type="submit" class="btn">
                                <i class="fa fa-btn fa-plus"></i>Add items
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Current products -->
            @if (count($product->items) > 0)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Current Items
                    </div>

                    <div class="panel-body">
                        <table class="table table-striped product-table">
                            <thead>
                                <th>Status</th>
                                <th>Physical status</th>
                                <th>Order information</th>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                            </thead>
                            <tbody>
                                @foreach ($product->items as $item)
                                    <tr>
                                        <td class="table-text"><div>{{ $item->status }}</div></td>
                                        <td class="table-text"><div>{{ $item->physical_status }}</div></td>
                                        <td class="table-text"><div>
                                                @if (isset($item->order->id))
                                                    <a href="{{url('orders/'.$item->order->id)}}">{{ $item->order->id }}</a>
                                                @else
                                                    --
                                                @endif
                                            </div></td>

                                        <!-- product Delete Button -->
                                        <td>
                                            @if (isset($item->order->id) && !$item->isDelivered())
                                                <form action="{{ url('items/detach_item/'.$item->id) }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger">
                                                        <i class="fa fa-btn fa-trash"></i>Detach from order
                                                    </button>
                                                </form>
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->isPendingDelivery())
                                                <form action="{{ url('items/deliver_item/'.$item->id) }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="fa fa-btn"></i>Mark as delivered
                                                    </button>
                                                </form>
                                            @elseif($item->isPendingItemOrder())
                                                <form action="{{ url('items/ordered_item/'.$item->id) }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="fa fa-btn"></i>Mark as ordered
                                                    </button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
