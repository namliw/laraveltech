<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Item;

class Product extends Model
{
    public function items()
    {
        return $this->hasMany(Item::class);
    }

    /**
     * creates a new product.
     * @param $request
     * @return bool
     */
    public static function createProduct($request)
    {
        try {
            $order = new Product;
            $order->sku = $request->sku;
            $order->name = $request->name;
            $order->colour = $request->colour;
            $order->save();
        } catch (\Exception $e) {
            //TODO: add logging
            return false;
        }
        return true;
    }

    /**
     * updates product attributes
     * @param $request
     * @return bool false on failure | true on success
     */
    public function updateProduct($request)
    {
        try {
            //marks if a call to save is necessary
            $update = false;

            if ($request->name != '' && $request->name != $this->name) {
                $this->name = $request->name;
                $update = true;
            }

            if ($request->colour != '' && $request->colour != $this->colour) {
                $this->colour = $request->colour;
                $update = true;
            }

            /**
             * If SKU is updated, update related items
             */
            if ($request->sku != '' && $request->sku != $this->sku) {
                /*
                 * Leaving this here just because is a technical test.
                 * For consolidation we should keep track of SKU updates (or not update at all) as it will
                 * throw off accounting.
                 */
                $this->sku = $request->sku;
                //update dependant items
                $this->items()->update(['product_sku' => $request->sku]);
                $update = true;
            }

            //save only if changes were made
            if ($update) {
                $this->save();
            }
        } catch (\Exception $e) {
            //TODO: add logging
            return false;
        }

        return true;
    }

    /*
     * Add items to product
     */
    /**
     * @param $qty - quantity must be integer
     * @param bool $p_status - physical status of the item, if none is provided default physical status will be assigned
     * @return bool - returns false if fails, true on success.
     */
    public function createItems($qty, $p_status = false)
    {
        //validate that the quantity is numeric
        if (!is_numeric($qty) && $qty < 1) {
            return false;
        }

        //load default physical status if none is provided
        if ($p_status == false) {
            $p_status = Item::DEFAULT_PHYSICAL_STATUS;
        }

        //item to store new items
        $items = array();

        //create new objects and store in array
        while ($qty > 0) {
            $it = new Item;
            $it->status = Item::DEFAULT_STATUS;
            $it->physical_status = $p_status;
            $it->product_sku = $this->sku;
            $items[] = $it;
            $qty--;
        }

        try {
            //save and associate all new items in a single call?
            $this->items()->saveMany($items);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * Create a new item if there's no one available during order creation
     *  returns false if fails or the new Item object on success
     * @return \App\Item|bool
     */
    public function createAndReturnItem()
    {
        try {
            $it = new Item;
            $it->status = Item::DEFAULT_STATUS;
            $it->physical_status = Item::DEFAULT_PHYSICAL_STATUS;
            $it->product_sku = $this->sku;
            $it->product()->associate($this);
            $it->save();
            return $it;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * returns an item that can be attached to an order.
     * @param $productSKU
     * @return Item class object or null if no item is available.
     */
    public static function getAvailableItem($productSKU)
    {
        return Item::where('product_sku', $productSKU)
            ->where('status', Item::DEFAULT_STATUS)
            ->first();
    }

    /**
     * Attempts to load an existing object from the database by SKU.
     * It will create a new product if none is found.
     * @param $productSKU
     * @return Product
     */
    public static function getOrCreateBySku($productSKU)
    {
        //check if product exists
        $product = Product::where('sku', $productSKU)
            ->first();

        //if product doesnt exist
        if (!isset($product)) {
            $faker = \Faker\Factory::create();
            //create it
            $product = new Product;
            $product->sku = $productSKU;
            $product->colour = $faker->colorName;
            $product->name = $faker->words(3,true);
            $product->save();
        }

        return $product;
    }

    /**
     * Adds a single item to an order.
     * @param $order
     * @param $productSKU
     * @param int $qty
     * @return bool
     */
    public static function addItemToOrder($order, $productSKU, $qty = 1)
    {
        if(!is_numeric($qty) || $qty < 0){
            return false;
        }

        //checks if the product has items available
        $item = Product::getAvailableItem($productSKU);

        //Either not available items or product doesn't exist
        if (!isset($item)) {
            //Load product or create if it doesnt exist
            $product = Product::getOrCreateBySku($productSKU);

            //create a new item to use with this order
            $item = $product->createAndReturnItem();//create a new item for this order
        }

        //associate item to order
        $item->attachItemToOrder($order, $qty);

        return true;
    }

    /**
     * deletes a product only if it's items are not assigned to any order.
     * @return bool
     */
    public function deleteProduct(){
        $items = $this->items()->where('status',Item::STATUS_NOT_AVAILABLE)->count();
        if (isset($items) && $items > 0){
            return false;
        }

        foreach ($this->items as $item){
            $item->delete();
        }
        $this->delete();

        return true;
    }

}
