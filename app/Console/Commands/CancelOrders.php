<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;

class CancelOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:cancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Marks orders as cancelled if all items where removed from it.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::where('status',Order::DEFAULT_STATUS)->get();//Todo: add limit to avoid loading large amounts of data
        $this->info('Process started');
        foreach($orders as $order){
            $res = $order->markAsCancelled();
            if($res === true){
                $this->info($order->id.': Cancelled.');
            }
        }
    }
}
