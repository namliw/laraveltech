<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;

class CompleteOrders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:complete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Loops orders and marks them as complete if all items have been delivered.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $orders = Order::where('status',Order::DEFAULT_STATUS)->get();//Todo: add limit to avoid loading large amounts of data
        $this->info('Process started');
        foreach($orders as $order){
            $res = $order->markAsCompleted();
            if($res === true){
                $this->info($order->id.': Completed.');
            }
        }
    }
}
