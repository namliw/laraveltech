<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

use App\Order;
use Illuminate\Http\Request;

//Default web routes
Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return view('orders', [
            'orders' => Order::orderBy('created_at', 'asc')->get()
        ]);
    });
    Route::get('/orders/{id}', 'OrderController@viewOrder');
    Route::delete('/order/{id}', 'OrderController@deleteOrder');

});

//Product action routes
Route::group(['middleware' => ['web'],'prefix' => 'products'], function () {

    Route::get('/', 'ProductController@listProducts');
    Route::get('/{id}', 'ProductController@viewProduct');

    Route::post('/', 'ProductController@createProduct');
    Route::post('/addItems/{id}', 'ProductController@addItems');
    Route::post('/update/{id}', 'ProductController@updateProduct');

    Route::delete('/{id}', function ($id) {
        $ses = \App\Product::findOrFail($id)->deleteProduct();
        $response = redirect('/products');

        if($ses === false){
            $response->withErrors('Product couldnt be deleted.');
        }

        return $response;
    });

});

//Item action routes
Route::group(['middleware' => ['web'],'prefix' => 'items'], function () {

    Route::get('/edit/{id}', 'ItemController@updateItem');
    Route::post('/detach_item/{id}', 'ItemController@detachItemFromOrder');
    Route::post('/deliver_item/{id}', 'ItemController@reportItemDelivery');
    Route::post('/ordered_item/{id}', 'ItemController@reportItemOrder');

    Route::delete('/{id}', function ($id) {
        Item::findOrFail($id)->delete();
        return redirect('/');
    });

});

//Api action routes
Route::group(['prefix' => 'api'], function () {
    
    Route::get('orders', 'OrderController@listOrders');
    Route::get('orders/{id}', 'OrderController@viewOrder');
    Route::post('orders/create', 'OrderController@create');
    Route::post('orders/{id}/remove_items', 'OrderController@removeItemsFromOrder');
    Route::post('orders/{id}/assign_items', 'OrderController@assignItemsToOrder');

});

#initial plan for routes
#Resource   URL            
#Orders     /api/orders     
#Orders     /api/orders/{id}
#Orders     /api/orders/{id}/remove_items
#Orders     /api/orders/{id}/remove_items/{item_id}
#Orders     /api/orders/{id}/assign_items
#Items      /api/items/
#Items      /api/items/{id}
#Items      /api/items/{id}/update
#Items      /api/items/{id}/modify_status/{new_status}

#php artisan make:migration create_orders_table --create=orders
#php artisan make:migration create_products_table --create=products
#php artisan make:migration create_items_table --create=items

#php artisan make:model Order
#php artisan make:model Product
#php artisan make:model Item