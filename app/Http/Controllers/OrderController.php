<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
#use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Order;

#use Input;

#Resource   URL            
#Orders     /api/orders     
#Orders     /api/orders/{id}
#Orders     /api/orders/{id}/remove_items
#Orders     /api/orders/{id}/remove_items/{item_id}
#Orders     /api/orders/{id}/assign_items

class OrderController extends Controller
{
    /**
     * shows all orders
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function listOrders()
    {
        return Order::all();
    }

    /**
     * Displays a single order
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewOrder($id)
    {
        $p = Order::findOrFail($id);
        return view('order', [
            'order' => $p
        ]);
    }

    /*
     * This method removes the association between items and the order.
     * It doesn't delete the items from the database. Items will become available and will be
     * used when placing new orders.
     * Expected JSON input format:
        {
            items: [ 'TESTSKU1','TESTSKU2']
        }
    */
    public function removeItemsFromOrder(Request $request, $id)
    {
        $data = $request->all();

        if (!isset($data) || !isset($data['items'])) {
            return response()->json([
                'success' => 'false',
                'message' => 'Invalid data received.'
            ]);
        }

        $p = Order::findOrFail($id);

        if(!$p->isInProgress()){
            return response()->json([
                'success' => 'false',
                'message' => 'This order cant be edited.'
            ]);
        }

        $res = $p->removeItemsFromOrder($data['items']);

        if($res === false ){
            return response()->json([
                'success' => 'false',
                'message' => 'Error removing items.'
            ]);
        }

        return response()->json([
            'success' => 'true',
            'message' => 'Items removed.'
        ]);
    }

    /**
     * Adds new items to an order.
     * existing SKUs (items previously attached) will be skipped.
     * This method doesn't remove old associations but rather creates new ones.
     * Expected JSON input format:
      {
          items: [ { sku: 'TESTSKU1',
                    quantity: 2 },
                  { sku: 'TESTSKU2',
                    quantity: 1 }
          ]
      }
     */
    public function assignItemsToOrder(Request $request, $id)
    {
        $data = $request->all();

        if (!isset($data) || !isset($data['items'])) {
            return response()->json([
                'success' => 'false',
                'message' => 'Invalid data received.'
            ]);
        }

        $order = Order::findOrFail($id);

        if(!$order->isInProgress()){
            return response()->json([
                'success' => 'false',
                'message' => 'This order cant be edited.'
            ]);
        }

        foreach ($data['items'] as $it) {
            Product::addItemToOrder($order, $it['sku'], $it['quantity']);
        }

        return response()->json([
            'success' => 'true',
            'message' => 'New items added.'
        ]);

    }

    /*
     * Creates a new order.
     * expected input in json format:
        {
            order: {
                    customer: 'Gabriel Jaramillo',
                      address: 'test address',
                     total: 100,
                     items: [ { sku: 'TESTSKU1',
                                quantity: 2 },
                              { sku: 'TESTSKU2',
                                quantity: 1 }
                            ]
                }
        }
    */
    public function create(Request $request)
    {
        $data = $request->all();
        if (!isset($data) || empty($data) || !isset($data['order'])) {
            return response()->json([
                'success' => 'false',
                'message' => 'Invalid data received.'
            ]);
        }

        $res = Order::placeOrder($data['order']);

        if (!is_numeric($res)) {
            return response()->json([
                'success' => 'false',
                'message' => 'Error while creating order.'
            ]);
        }

        return response()->json([
            'success' => true,
            'order_id' => $res,
            'message' => 'Order Processed.'
        ]);
    }

    public function deleteOrder($id){
        $res = Order::findOrFail($id)->deleteOrder();
        $response = redirect('/');
        if($res === false){
            $response->withErrors('Order could not be deleted.');
        }
        return $response;
    }

}


