<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product;
use Validator;

class ProductController extends Controller
{
    /**
     * List all existing products
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listProducts(){
    	return view('products', [
            'products' => Product::orderBy('created_at', 'asc')->get()
        ]);
    }

    /**
     * creates a new product
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function createProduct(Request $request){
        //TODO:validate
		$rer = redirect('/products');
    	$response = Product::createProduct($request);
    	if($response != true){
    		$rer->withInput()
                ->withErrors('An error has ocurred'.$response);
    	}
    	return $rer;
    }

    /**
     * Updates a new product attributes
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateProduct(Request $request,$id){
        $p = Product::findOrFail($id);
        $rer = redirect('/products/'.$id);
        $response = $p->updateProduct($request);

        if($response !== true){
            $rer->withInput()
                ->withErrors('An error has occurred '.$response);
        }

        return $rer;
    }

    /**
     * Displays a single product
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewProduct(Request $request,$id){
        $p = Product::findOrFail($id);

        return view('product', [
            'product' => $p
        ]);
    }

    /**
     * Creates new items for an existing product
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function addItems(Request $request,$id){
    	$qty = $request->items;
    	$p = Product::findOrFail($id);
    	$res = $p->createItems($qty,$request->physical_status);

        if($res){
            return redirect('/products/'.$id);
        }

        $rer = redirect('/products');
        $rer->withInput()
            ->withErrors('An error has ocurred while adding the items.');
        return $rer;
    }
}
