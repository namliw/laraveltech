<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Item;

#Items      /api/items/
#Items      /api/items/{id}
#Items      /api/items/{id}/update
#Items      /api/items/{id}/modify_status/{new_status}

class ItemController extends Controller
{
    /**
     * removes the association between an order and an item.
     * this does not delete the item from the database.
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function detachItemFromOrder($id)
    {
        $response = back();
        try {
            $it = Item::findOrFail($id);
            $res = $it->detachFromOrder();
            if($res === false){
                $response
                    ->withErrors('Could not detach Item.');
            }
        } catch (\Exception $e) {
            $response
                ->withErrors('An error has ocurred: ' . $e->getMessage());
        }

        return $response;
    }

    /**
     * Updates an item status from "in wharehouse" to "delivered"
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function reportItemDelivery($id)
    {
        $it = Item::findOrFail($id);
        if ($it->markAsDelivered() === false) {
            return back()
                ->withErrors('An error has ocurred.');
        }

        return back();
    }

    /**
     * Updates an item physical status from "to order" to "In wharehouse"
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function reportItemOrder($id)
    {
        $it = Item::findOrFail($id);
        if ($it->markAsOrdered() === false) {
            return back()
                ->withErrors('An error has ocurred.');
        }

        return back();
    }
}
