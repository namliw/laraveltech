<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Order;
use App\Product;

class Item extends Model
{
    //Item statuses
	const DEFAULT_STATUS = 'Available';//When in this status item can be attached to order
    const STATUS_NOT_AVAILABLE = 'Assigned';//when in this status, item is attached to an order

	const DEFAULT_PHYSICAL_STATUS = 'To order'; //Item is out of stock
    const DELIVERED_PHYSICAL_STATUS  = 'Delivered';//Item was delivered to customer
    const WAREHOUSE_PHYSICAL_STATUS  = 'In warehouse'; //Item is in stock

    public function order(){
    	return $this->belongsTo(Order::class,'order_id');
    }

    public function product(){
    	return $this->belongsTo(Product::class);	
    }

    /**
     * true: item is attached to an order and in stock.
     * false: item is either out of stock or not associated with an order.
     * @return bool
     */
    public function isPendingDelivery(){
        return $this->physical_status == self::WAREHOUSE_PHYSICAL_STATUS
            && $this->status == self::STATUS_NOT_AVAILABLE;
    }

    /**
     * true: item is out of stock
     * false: item is in stock.
     * @return bool
     */
    public function isPendingItemOrder(){
        return $this->physical_status == self::DEFAULT_PHYSICAL_STATUS;
    }

    /**
     * true: item was delivered to customer
     * false: item hasnt been delivered to the customer, but is associated with an order
     * @return bool
     *
     */
    public function isDelivered(){
        return $this->physical_status == self::DELIVERED_PHYSICAL_STATUS
            && $this->status == self::STATUS_NOT_AVAILABLE;
    }

    /**
     * Changes physical status to "delivered'
     * @return bool
     */
    public function markAsDelivered(){
        //item cant be delivered if it's not assigned
        if($this->status == self::DEFAULT_STATUS){
            return false;
        }
        $this->physical_status = self::DELIVERED_PHYSICAL_STATUS;
        return $this->save();
    }

    /**
     * Changes physical status to "in warehouse"
     * @return bool
     */
    public function markAsOrdered(){
        $this->physical_status = self::WAREHOUSE_PHYSICAL_STATUS;
        return $this->save();
    }

    public function isAvailable(){
        return $this->status == self::DEFAULT_STATUS;
    }

    /**
     * Associates items to order
     * @param $order
     * @param $qty
     * @return bool
     */
    public function attachItemToOrder($order,$qty = 1){
        //only attach item if it's available
        if(!$this->isAvailable()){
            return false;
        }
        $this->status = Item::STATUS_NOT_AVAILABLE;
        $this->quantity = $qty;
        $this->order()->associate($order);
        $this->save();
        return true;
    }

    /**
     * detaches an item from it's order
     */
    public function detachFromOrder(){
        //Detaching delivered items is forbidden
        if($this->isDelivered()){
            return false;
        }
        if(!isset($this->order) || !$this->order->isInProgress()){
            return false;
        }
        $this->order()->dissociate();
        $this->status = self::DEFAULT_STATUS;
        $this->save();
        return true;
    }

}
