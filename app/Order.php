<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use App\Item;

class Order extends Model
{
    const DEFAULT_STATUS = 'In progress';
    const COMPLETED_STATUS = 'Completed';
    const CANCELLED_STATUS = 'Cancelled';

    protected $fillable = ['customer_name','address','status'];

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    /**
     * This method removes the association between an item and an order.
     * It doesnt delete the item from the database.
     * Item will be reused when creating new orders through the API.
     * @param $items
     * @return bool
     */
    public function removeItemsFromOrder($items)
    {
        //Only execute if order is in progress.
        if (!$this->isInProgress()) {
            return false;
        }

        //load all items that match the SKUs sent for removal.
        //Ignore delivered items
        $itemsToRemove = $this->items()->whereIn('product_sku', $items)
            ->where('physical_status','<>',Item::DELIVERED_PHYSICAL_STATUS)->get();

        //if there's items to process
        if (!empty($itemsToRemove)) {
            try {
                //remove association from order
                foreach($itemsToRemove as $item){
                    $item->detachFromOrder();
                }
            } catch (\Exception $e) {
                return false;
            }
        }
        return true;
    }

    /**
     * Adds new items to order.
     * Ignores previous associations.
     * @param $items
     * @return bool
     */
    public function attachItemsToOrder($items)
    {
        //Execute only if order is in progress
        if (!$this->isInProgress()) {
            return false;
        }
        //load current items skus
        $currentItems = $this->items()->pluck('product_sku')->toArray();

        //loop through the request skus
        foreach ($items as $it) {
            //if an association exist, skip
            if(!in_array($it['sku'],$currentItems)){
                continue;
            }

            //attach item to order
            Product::addItemToOrder($this, $it);
        }

        return true;
    }

    /**
     * Creates a new order from the API call
     * @param $data
     * @return mixed
     */
    public static function placeOrder($data)
    {
        //Create new order
        $order = Order::create([
            'customer_name' => $data['customer'],
            'address' => $data['address'],
            'status' => self::DEFAULT_STATUS,
        ]);

        $its = $data['items'];

        foreach ($its as $it) {
            Product::addItemToOrder($order, $it['sku']);
        }

        return $order->id;
    }

    public function isCompleted()
    {
        return $this->status == self::COMPLETED_STATUS;
    }

    public function isInProgress()
    {
        return $this->status == self::DEFAULT_STATUS;
    }

    public function isCancelled()
    {
        return $this->status == self::CANCELLED_STATUS;
    }

    /**
     * Marks an order as completed if all items have been delivered
     * @return bool
     */
    public function markAsCompleted(){
        if($this->status != self::DEFAULT_STATUS){
            return false;
        }
        $itCount = $this->items()->count();
        $itDelivered = $this->items()->where('physical_status',Item::DELIVERED_PHYSICAL_STATUS)->count();
        if($itCount != 0 && $itDelivered != 0){
            if($itCount == $itDelivered){
                $this->status = self::COMPLETED_STATUS;
                $this->save();
                return true;
            }
        }
        return false;
    }

    /**
     * Mark an order as Cancelled if all items have been removed from items
     * @return bool
     */
    public function markAsCancelled(){
        if($this->status != self::DEFAULT_STATUS){
            return false;
        }
        $itCount = $this->items()->count();
        if($itCount == 0){
            $this->status = self::CANCELLED_STATUS;
            $this->save();
            return true;
        }
        return false;
    }

    /**
     * Deletes an order. Only cancelled order can be deleted.
     * @return bool
     */
    public function deleteOrder(){
        //only delete cancelled orders
        if(!$this->isCancelled()){
            return false;
        }
        $this->delete();
        return true;
    }
}
